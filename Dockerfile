# https://github.com/dwurf/docker-kf2/blob/master/Dockerfile

FROM steamcmd/steamcmd:latest

MAINTAINER Lexi Micham <opensource@leximicham.com>

# download requirements and then clean up
RUN apt-get update && \
	apt-get -y install wget lib32gcc1 libcurl4 && \
	apt-get clean && \
	find /var/lib/apt/lists -type f | xargs rm -vf
