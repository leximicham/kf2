#!/bin/bash

#run this with sudo crontab -e
#replace xx with the hour of the day you want 15-30 minutes of downtime
#0 xx * * * bash /absolute_path_to_nightly.sh

#Set cronjob's directory to the directory of the nightly.sh file for environment variables and docker-compose
cd `dirname $0`

#stop the server and clean up orphaned/anonymous images, networks, containers, and volumes
docker-compose down --rmi all --remove-orphans

#restart and rebuild in case there are changes to the Dockerfile/docker-compose.yml
docker-compose up -d --build --force-recreate
