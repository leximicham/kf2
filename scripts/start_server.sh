#!/bin/bash

# update server's data
steamcmd \
	    +login anonymous \
	        +force_install_dir /kf2/game \
		    +app_update 232130 \
		        +quit

#Copy 64bit steamclient, since it keeps using 32bit
cp /root/.steam/steamcmd/linux64/steamclient.so /kf2/game/

#Launch server with arguments from the environment variables piped into the Dockerfile
/kf2/game/Binaries/Win64/KFGameSteamServer.bin.x86_64 kf-bioticslab?GamePassword="$KF2_GAME_PASS"?AdminName="$KF2_ADMIN_NAME"?AdminPassword="$KF2_ADMIN_PASS"?Port="$KF2_GAME_PORT"?QueryPort="$KF2_QUERY_PORT"
